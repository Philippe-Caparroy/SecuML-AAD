import matplotlib
matplotlib.use('Agg')
import sys

from SecuML.core.tools import colors_tools
from SecuML.exp.active_learning.ActiveLearningConf import ActiveLearningConf
from SecuML.exp.active_learning.ActiveLearningExperiment \
        import ActiveLearningExperiment




from SecuML.core.tools.core_exceptions import SecuMLcoreException
from SecuML.exp.tools.exp_exceptions import SecuMLexpException

if __name__ == '__main__':
    try:
        parser = ActiveLearningConf.generateParser()
        args = parser.parse_args()
        exp_conf = ActiveLearningConf.fromArgs(args)
        exp = ActiveLearningExperiment(exp_conf)
        exp.run()
        exp.close()
    except (SecuMLcoreException, SecuMLexpException) as e:
        sys.stderr.write(colors_tools.display_in_red(e) + '\n')
        exp.rollbackSession()
        sys.exit(-1)
    except Exception as e:
        exp.rollbackSession()
        raise(e)
